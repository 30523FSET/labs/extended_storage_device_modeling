'''
Single home with a TOU tarriff. The network is essentially the user's home. There is only one objective
- maximize the user's pay off. The user has a "Retail" device which represents their connection to the
retailer. It can be considered like a DG owned by the user with a funny cost function.
'''
import sys
import numpy as np
import device_kit
from device_kit import *
from powermarket.scenario.lcl.lcl_scenario import irandom, make_gas_gen
import random
from logging import debug, info

meta = {
  'title': ""
}


def make_deviceset(battery=True, pv=False, **kwargs):
  random.seed(23)
  debug(kwargs)
  load = np.array([0.484, 0.462, 0.412, 0.313, 0.321, 0.403, 0.606, 1.044, 0.772, 0.728, 0.953, 1.273, 1.467, 1.754, 1.411, 1.29 , 1.207, 1.292, 1.454, 1.688, 1.553, 1.417, 1.219, 0.784])
  devices = [
    Device('load', 24, np.stack((load, load), axis=1)),
    # make_gas_gen(1, 'supply'),
    ADevice('supply', 24, (-20, 0), **{'f': Retail()})
  ]
  if battery:
    devices.append(SDevice('battery',
      length=24,
      bounds=[-2,2],
      capacity=8,
      c1=0.,
      **kwargs
    ))
  return DeviceSet('home', devices, sbounds=(0,0))


class Retail():
  ''' Callable cost function suitable for ADevice.f:

    fixed: N/A ($/d)
    demand: X ($/kW/d)
    volume:
      off: {[0-6, 22-23], 19.7}
      shoulder: {[7-14, 20-21], 25.4}
      peak: {[15-19], 47.40}
    blocks: {"<= XkW": 1, "else": Y}

  With the block rate volume charge is a continuous but not c.d increasing p.w. linear function.
  The demand charge component is also continuous but not c.d.

  __call__(), deriv(), and hess() all expect to be called with a -ve number.
  '''
  tou_price = np.hstack((np.ones(7)*0.197, np.ones(8)*0.254, np.ones(5)*0.474, np.ones(2)*0.254, np.ones(2)*0.197))
  demand_price = 0.30
  block_factor = 1.5
  block_start = 2

  def __init__(self):
    pass

  def __call__(self, r):
    ''' Return the utility (-ve the cost) of supplying r. '''
    return -self.cost(-r)

  def deriv(self):
    return lambda r: self._deriv(-r)

  def _deriv(self, r):
    ''' np.choose is a confusing function. np.choose(r < 2, ['N', 'Y']) '''
    volume_deriv = np.choose(r < self.block_start, [self.tou_price*self.block_factor, self.tou_price])
    demand_deriv = np.choose(r < np.max(r), [self.demand_price, 0])
    return volume_deriv + demand_deriv

  def hess(self):
    return nd.Hessian(lambda r: np.zeros((len(r), len(r))))

  def cost(self, r):
    ''' r is expected to be a +ve number '''
    return self.costs_volume(r).sum() + self.costs_demand(r).sum()

  def costs_demand(self, r):
    ''' Pretend demand charges are per interval so we can show how usage effects costs. '''
    return np.choose(r < np.max(r), [self.demand_price, 0])*r

  def costs_volume(self, r):
    return np.minimum(r, self.block_start)*self.tou_price + np.maximum(r - self.block_start, 0)*self.tou_price*self.block_factor

  def to_dict(self):
    return {}
