import numpy as np
import matplotlib.pyplot as plt


charge1 = lambda x, e=0.7: ((e**np.sign(x))*x)
charge = lambda x, e=0.7: ((e**np.sign(x))*x).cumsum()
ax = np.linspace(-1,1,100)
plt.plot(ax, np.vectorize(charge1)(ax))
plt.xlabel('RoC')
plt.ylabel('Change in SoC')
# plt.title('Effect on SoC with efficiency factor of 0.7')
plt.xlim(-1,1)
plt.grid(True)
#plt.show()
plt.savefig('img/efficiency-factor-2.png')
