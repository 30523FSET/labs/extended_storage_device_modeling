'''
'''
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fminbound
from logging import debug


charge = lambda x, e=0.7: ((e**np.sign(x))*x).cumsum()
colors = ['red', 'orange', 'yellow', 'purple', 'fuchsia', 'lime', 'green', 'blue', 'navy', 'black']
j = 0
t = 0

np.random.seed(3)

while j < 1:
  x = np.random.random(20)-0.4
  y = np.random.random(20)-0.4

  (cnx, cny) = (charge(x)[-1], charge(y)[-1])
  (mx, my) = (np.max(charge(x)), np.max(charge(y)))
  m = max(mx, my)

  # Let min SoC be zero.
  if min(charge(x)) < 0. or min(charge(y)) < 0.:
    continue
  j += 1

  print(np.round(charge(x),3))
  print(np.round(charge(y),3))

  # The line between x and y. We need to be able to find an upper bound SoC values on this line
  # apriori.
  l = lambda a: charge((1-a)*x + a*y)[-1]
  line = np.vectorize(l)
  ax = np.linspace(0, 1, 100)
  p = l(fminbound(lambda v: -1*l(v), 0, 1, disp=0, maxfun=1e4, xtol=1e-08))

  print(p > m + 2e-6, p, '>', m)
  t += 1 if p > m + 2e-6 else 0

  plt.plot(ax, line(ax), label='s((1-a)x + ay)', ls='-', color=colors[j%10])
  plt.axhline(p, ls='--', color=colors[j%10], label='max(s((1-a)x + ay))')
  plt.axhline(max(mx,my), ls='-.', color=colors[j%10], label='s_max')

print('t %d/%d' % (t, j))
plt.legend()
plt.xlabel('a')
plt.ylabel('s((1-a)x + ay')
plt.show()
