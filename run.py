#!/usr/bin/env python3
import itertools
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import device_kit
from scenario import make_deviceset
import logging
from logging import debug, info
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


for s in [(False, None, None)] + list(itertools.product([True], [None, 2], [1.0, 0.95, 0.9, 0.8])):
  debug('-'*100)
  (battery, rate_clip, efficiency) = s
  model = make_deviceset(battery=battery, efficiency=efficiency, rate_clip=rate_clip)
  (x, solve_meta) = device_kit.solve(model, p=0, solver_options={'ftol': 1e-6})
  debug(solve_meta.message)
  result = dict(model.map(x))
  supply = model.get('supply')
  df = pd.DataFrame.from_dict(result, orient='index')
  df.loc['total'] = df.sum()
  pd.set_option('display.float_format', lambda v: '%+0.3f' % (v,),)
  cost = -supply.u(df.loc['home.supply'].values, 0)
  total = -df.loc['home.supply'].values.sum()
  print('|%s|%s|%.2f|%.2f|' % ('%.2f' % efficiency if battery else '-', str(rate_clip) if battery else '-', cost, total))
  #print(df.sort_index())
  #print(df.sum(axis=1))
