---
layout: default
permalink: /
---
<style>
  table { width: 100%;}
  td { min-width: 50px; text-align: center; vertical-align: top; }
  figure {display: inline-block; margin: 0}
</style>
# A Brief Analysis of Storage Device Models Suitable for Day-ahead Optimal Economic Scheduling Problems

# Introduction
The future smart grid will be a distributed system of systems and the building block of that system of systems is the intelligent autonomous microgrid. Small scale distributed energy storage will play a vital role in the future microgrid and smart grid. Distributed energy storage offers several important benefits:

  - Enables shifting power consumption to low cost times, such as within the envelop of non dispatchable renewable energy generation.
  - Reduces peak demand.
  - Increased reliability by supporting islanded operation of small networks or individual sites.

In fact, some kind of fast acting electrical storage is considered a practically essential in robust dynamically island-able microgrids to counter a lack of stability and increased volatility.

To evaluate the high level day to day economic operation of a microgrid (also sometimes called embedded energy network) we build abstract models of it. Since energy storage is such an important part of the picture, we need a suitable realistic model of energy storage that we can *plugin* to the microgrid model. The mode should be intuitive and simple to understand, but at the same time be able represent real life batteries to a "close enough" approximation for economic modelling. This report reviews existing battery modeling and then based on this work defines a suitably fully featured energy storage device model for use in future microgrid modeling scenarios.

# Battery Modeling Review
A battery is one very common type of electrical energy storage device. Many examples of battery modeling exist in the literature. Batteries come in many different shapes and sizes, are implemented with different technologies and are used in many different application areas. The appropriate modeling abstraction depends on the application area, the questions one wishes to answer, and what level of precision is required in those answers.

[Gu et al][Gu2014] categorize existing mathematical battery modeling attempts into three classes:

  1. Chemical mechanism modeling
  2. Circuit equivalent modeling
  3. Numerical modeling

The complexity in the first two classes of modeling makes them impractically complicated for use in answering broad questions about the economic performance of a battery coupled to a microgrid environment. [Jongerden et al][Jongerden2009] puts it well:

> "Although these models describe the battery accurately, they are not suitable to be used in the setting of the performance models because of the detailed description, which would make the combined model unmanageable. What is needed is an abstract model that focuses on the important battery properties and their effects only."

Numerical models are the most abstract and focus only on the key performance characteristics of a battery as a source and sink of electrical energy over time, and are most suitable for the type of high economic analysis we are interested in.

The most well known numerical battery models are the kinetic battery model (KiBaM) and the so-called diffusion model are the two most well known [Jongerden et al][Jongerden2009]. Yet even these simple models may not be fit for our purposes because they do not fit well with the way the rest of the domain is modeled. For evaluating the high level day to day economic operation of a battery, or more generally an energy storage device, in a small electrical network such as a microgrid, the key performance characteristic is essentially how much money it costs to operate and how much money it saves. We cannot evaluate these performance measures of a battery without determining the schedule of battery charge/discharge profile is, under a specific setting (base demand, demand elasticity, costs, supply prices, tariff structures).

## Energy Storage Device for Day-to-day Economic Performance Analysis
To evaluate the day to day economic operation of a *energy storage device* (which is not necessarily a chemical battery) we essentially need to solve an day-ahead optimal scheduling problem, where the energy storage device is just one of many schedulable or unschedulable devices present in a microgrid (or w.l.o.g. small electrical network such as a house). Essentially, we need to determine the optimal in-flows and out-flows of energy from the device in collaboration with all other devices, and from some perspective of what is "optimal". In many previous works such as [Li et al][Li2011], [Xiaoping et al][Xiaoping2010], batteries are simply modeled as a lossless buffer for electrical energy operating over some short future time horizon of discrete time intervals. Such a model is simple and intuitive and works well for economic optimization. However a lossless buffer model overlooks important characteristics common to many energy storage devices that might be present on a microgrid that may have a significant effect economic outcomes.

While we could refine the model ad infinitum, there are three major short comings of the common model of a battery as a lossless buffer:

  1. *Round trip efficiency factor:* Efficiency factor often significant for electrical storage devices and many authors include efficiency factor in the battery modeling [Zhang][Zhang2013Robust], [needs-more-citations](#), although exactly how this is done varies.
  2. *Self discharge:*  It is less common to model self discharge because it is not significant for the most commonly encountered chemical battery technologies in day-ahead time frames. However it may be significant for other electrical storage technologies such as flywheels and super capacitors. More over, self discharge (as well as efficiency) tend to become more significant for heat and coolth energy storage devices which we need to model when considering the *total energy system* not just electricity.
  3. *SoC dependent RoC:* It seems intuitive that the RoC (Rate of Charge) will vary with the SoC (State of Charge). This is indeed the case with most chemical batteries. [Wei et al][Wei2018] used simple two piece linear function to capture this SoC dependent RoC for the very common CC-CV charging regime used in most modern Lithium-Ion batteries. Wei et al were only interested in, and thus only modeled the charging cycle, but it stands to reason the SoC dependent RoC should apply to discharge too.

### Proposed Battery Model
A more complete discrete time finite horizon battery dynamics model is as follows. Let $T = \{1..n}$ be the number of time intervals in the scheduling horizon. Let $Q\_d$ be the energy flow vector of the storage device with negative values indicating flow out of the device, and $Q\_{d,t}$ the scalar flow time interval $t$. $S_{d,t}$ is the SoC (state of charge) of the battery at time $t$. The battery is described by the following parameters:

  + $Q\_{d}^{min}$, $Q_{d}^{max}$ are the min/max RoC (rate of charge) of the battery.
  + $S_d^{max}$ is the capacity of the battery.
  + $S_{d,0}$ is the initial SoC.
  + $S_d^{end}$ is the SoC required at the end of the scheduling horizon.
  + $f\_d \in [0,1]$ is the efficiency factor.
  + $l\_d \in [0,1]$ is the self discharge rate.
  + $k_d$ a scaling factor to convert to input/output energy units to internal energy state units. For an electrical storage device $k\_d$ has a value of 1. For a thermal mass acting as an energy store it may have a different value.
  + $c\_d^{min}$, $c\_d^{max} \in [1,\infty)$ control the SoC as a proportion $S\_d^{max}$ at which point the max RoD / RoC start to tail off linearly from $Q\_{d}^{min}$, $Q_{d}^{max}$ (see figure below).

The battery dynamics are defined by the following constraints:

$$
\begin{align}
\begin{split}
  \hat{Q_{d,t}^{min}} & \le Q_{d,t} \le \hat{Q_{d,t}^{max}} \ & \forall_t \in \{0..n\} \label{eq-di}\\
  0 & \le S_{d,t} \le S_{d}^{max}\ & \forall_t \in \{0..n\}\\
  0 & \le S_d^{end} \le S_{d,n} \le S_{d}^{max}\\
  0 & \le S_{d,0} \le S_{d}^{max}\\
\end{split}
\end{align}
$$

where:

$$
\begin{align}
\begin{split}
  S_{d,t} & = l_d S_{d,t-1} + Q_{d,t}f_d^{sign(Q_{d,t})}\\
  & = {l_d}^t S_{d,0} + \sum_{i=1}^{t}{l_d}^{(t-i)} Q_{d,i}f_d^{sign(Q_{d,i})} \ & \forall_t \in \{1..n\} \label{eq-dir}\\
\end{split}
\end{align}
$$

and $\hat{Q_{d,t}^{min}}$ and $\hat{Q\_{d,t}^{max}}$ are functions of $S\_{d,t}$:

$$
\hat{Q_{d,t}^{min}}(S_{d,t}; Q_d^{min}, c_d^{min}) = max(Q_d^{min}, c_d^{min} \frac{S_{d,t}}{S_d^{min}})
$$

$$
\hat{Q_{d,t}^{max}}(S_{d,t}; Q_d^{max}, c_d^{max}) = min(Q_d^{max}, c_d^{max} (1-\frac{S_{d,t}}{S_d^{max}}))
$$

We assume the energy losses due to the efficiency factor $f\_d$ occur in equal parts on input and output of a device.

The figure below shows the SoC variable RoC with $c_{d,min} = c_{d,max} = 2$

<img name="f8" src="img/dyn-roc-1.png" /><br>

#### Convexity Analysis
Ideally we'd like to be able to show the above device constraints are convex. Our approach is to start by showing the unextended model is convex then introducing each of the three extensions defined above and attempting to show the constraint sets remain convex. The unextended battery constraints are:

$$
\begin{align}
\begin{split}
  Q_{d}^{min} & \le Q_{d} \le Q_{d}^{max} \\
  0 & \le S_{d,0} + \sum_{i=1}^{t} Q_{d,i} \le S_{d}^{max}\ & \forall_t \in \{0..n\}\\
  S_d^{end} & \le S_{d,0} + \sum_{i=1}^{n} Q_{d,i} \le S_{d}^{max}\\
\end{split}
\end{align}
$$

where $Q_{d}^{min}$ and $Q_{d}^{max}$ are vector constraints. The first constraint is hyper-rectangle and thus convex. The second and third constraints are the intersection of a number of lower up bound constraints on linear combinations of $Q_d$ (simplexes) and thus also convex.

**Self-discharge factor ($l_d$):**<br>
The first constraint on min/max RoC is unaffected. The second and third become:

$$
\begin{align}
\begin{split}
  0 & \le {l_d}^t S_{d,0} + \sum_{i=1}^{t}{l_d}^{(t-i)} Q_{d,i} \ & \forall_t \in \{0..n\}\\
  S_d^{end} & \le {l_d}^n S_{d,0} + \sum_{i=1}^{n}{l_d}^{(n-i)} Q_{d,i} \le S_{d}^{max}\\
\end{split}
\end{align}
$$

For constant $l\_d$, and any fixed $n$, the $l_d^{t-i}, l_d^{n}$ terms are constant. Thus constraints are still the intersection of a number of lower up bound constraints on (now weighted) linear combinations of $Q_d$ (simplexes) and thus still also convex.

**SoC dependent RoC:**<br>
The expressions for $\hat{Q_{d,t}^{min}}$ and $\hat{Q_{d,t}^{max}}$ are equivalent to the intersection of the following constraints:

$$
\begin{align}
\begin{split}
  Q_{d}^{min}                         & \le Q_{d}\  & \le Q_{d}^{max} \\
  c_d^{min} \frac{S_{d,t}}{S_d^{min}} & \le Q_{d,t}\ & \le c_d^{max} (1-\frac{S_{d,t}}{S_d^{max}})\, & \forall_t \in \{1..n\}\\
\end{split}
\end{align}
$$

Clearly the first constraint is convex. The second constraint can be rewritten, $\forall_t \in \{1..n\}$:

$$
\begin{align}
\begin{split}
  (\frac{c_d^{min}}{S_d^{min}})S_{d,t} \le Q_{d,t}\ \cap\ Q_{d,t} \le c_d^{max} - (\frac{c_d^{max}}{S_d^{max}})S_{d,t}\\
  (\frac{c_d^{min}}{S_d^{min}})S_{d,t} - Q_{d,t} \le 0\ \cap\ 0 \le c_d^{max} - (\frac{c_d^{max}}{S_d^{max}})S_{d,t} - Q_{d,t}\\
\end{split}
\end{align}
$$

Recall from the first proof above, at each $t$, $S\_{d,t}$ as a function of $Q_d$ is a linear combination of the components of $Q\_d$. In the form above it can be seen we just have the intersection of two simplex constraints, which is convex.

**Efficiency factor ($f_d$):**<br>
Setting $l\_d = 1$ to simplify analysis, with the efficiency factor included the expression for $S_{d,t}$ is:

$$
\begin{align}
\begin{split}
  S_{d,t} & = S_{d,t-1} + Q_{d,t}f_d^{sign(Q_{d,t})}\\
  & = S_{d,0} + \sum_{i=1}^{t} Q_{d,i} f_d^{sign(Q_{d,i})} \ & \forall_t \in \{1..n\}
\end{split}
\end{align}
$$

Essentially we need to show the following two inequalities still hold when we include an efficiency factor (that is not equal to 1):

$$
\begin{align}
S_d^{end} & \le S_{d,t} \le S_{d}^{max}
\end{align}
$$

Consider the following scalar function:

$$
x f^{sign(x)} = \left\{
        \begin{array}{ll}
            \frac{x}{f}, & \quad x < 0\\
            x f, & \quad x \ge 0\\
        \end{array}
    \right.
$$

for $x \in (-k,k)$ some positive scalar k as shown in the figure below for $f=0.5$. When $f \in [0,1]$ the function is quasi-convex. For any convex function, $C(x):\mathbb{R}^n \rightarrow \mathbb{R}$, \{x| C(x) \le k\\}$ is a convex set, and this holds for quasi-convex function too. Unfortunately the summation of many such functions is not guaranteed to be quasi-convex, when $Q_{d,t}$ is free to range over positive and negative values. And we also have two inequalities ... Experimentally we can show the contraint is not convex:

<table>
<tr>
    <td>
      <figure>
        <a name='f1'><img style="width:480px" name='f1' src='img/efficiency-factor-2.png'/></a><br>
        <small>Shows how efficiency factor of 0.7 effects SoC as RoC varies between `[-1,1]`. The SoC is a quasi-convex function of RoC for `f=[0,1]`.</small>
      </figure>
    </td>
    <td>
      <figure>
        <a name='f2'><img style="width:480px" name='f2' src='img/efficiency-factor-3.png'/></a><br>
        <small>Shows SoC as a function of RoC for `f=0.7` for all points on the line between two random RoC vectors with length 20. The sum of many quasi-convex functions is not quasi-convex, and indeed often concave. We'd need to derive a sufficiently large max SoC such that all points on the line `(1-a)x + ay` for  `a = [0,1]` are feasible, while max SoC is still attainable. This is impossible without introducing new constraints on RoC in some way.</small>
      </figure>
    </td>
</tr>
</table>


**Summary of Convexity Analysis**<br>

  - Self discharge factor: Convex.
  - RoC dependent Soc: Convex.
  - Efficiency Factor: Not Convex.\*

\* The question is what is the consequence of the efficiency factor not being strictly convex? The constraint only comes into play when SoC is at or close to the upper bound. The non convexity also increases as $f$ decreases from 1.0, being convex with $f=1.0$. With a mild $f$ value and large upper bound the non-convexity may not have any effect while still being a useful addition to the modeling. But this may also depend on what optimization methods are used. More research needs to be done into the exact consequences and whether the issue can be resolved by some means. In the mean time, the following numerical analysis shows the constraint can be successfully represented and worked with within SciPy minimization library's SLSQP solver, and still provides meaningful and logical results without optimization errors. The risk is that the result returned is actually a local minima.

## Modeling Costs
Note although we have defined the feasible states of operation of the battery, no preference has so far been given to any one state. Various cost functions that reflect amortized maintenance and degradation costs can be included. However, it is harder to make general statements about which cost functions are appropriate because the exact costs tend to vary widely between technologies and operating environment. [Li et al][Li2011] provide a convex cost function with three terms to represent degradation costs to do with fast RoC, deep discharging, and frequent flip-flopping between charging and discharging. [Zhang et al][Zhang2013Robust] also included a cost to prevent deep discharging in a generic battery model. [Wei et al][Wei2018] derived a cost function to penalize high cost operating modes for Lithium Ion car batteries. Ideally an accurate cost function should be derived from empirical observations for a particular technology and environment. However the cost function is derived we are usually motivated to keep the cost functions relatively simple trading off representability for simplicity, comprehension and solvability. In particular if convex optimization is to be employed a convex approximate representation of the costs is highly desirable.

# Numerical Experiments
In order to give some idea how significant are these additional factors can be (and also to validate our implementation) we tested the battery model with various settings in single residential household test scenario. Only efficiency factor and rate clipping (i.e. SoC dependent RoC) were tested, as self discharge is typically negligible for a household battery (further testing will be done on self discharge with a different more suitable scenario). Power is in units of kW and energy in kWh. The scheduling interval is 1H and scheduling starts at mid-night. Simulation was done with [device_kit](https://gitlab.com/30523FSET/device_kit) using SciPy minimize with the [SLSQP](http://www.pyopt.org/reference/optimizers.slsqp.html) solver. The code is available [here](scenario.py).

## Test Site
The load profile was taken from an Australian student household with 5 residents. The site has no air-conditioning and has gas hotwater system. To account for this energy usage HVAC and heating load was added to the load profile derived from [Li et al][Li2011]. Total load is 24.3kW. There is *no* demand response capability besides a battery.

**Tariff:** The household facing a hypothetical modern variable rate tariff, with a TOU (time of use) volume charge, a demand charge, and an increasing 2-block volume rate charge. The block rate and demand charge are fairly extreme especially for a residential tariff. However, in the future it is anticipated these features will be more common in residential tariffs.

    fixed: N/A ($/d)
    demand: 0.30 ($/kW/d)
    volume:
      off: {[0-6, 22-23], 19.7}
      shoulder: {[7-14, 20-21], 25.4}
      peak: {[15-19], 47.40}
    blocks: {"<= 2kW": 1, "else": 1.5}

**Battery:** The battery parameters are as follows:

  + $(Q\_{d}^{min}, Q_{d}^{max}) = (-2,2)$
  + $(S\_d^{min}, S_d^{max} = (0,8)$
  + $S\_{d,0} = S_d^{end} = 4$
  + $l\_d = 0$

No battery cost function was used and $f\_d$, $c\_d^{min}$, $c\_d^{max}$ varied according to the table below which also list the total cost to the household under each variation.

## Results
Key findings:

  - With a perfectly efficient battery savings are significant at 22.7% but decrease roughly in proportion to increasing efficiency factor.
  - The increased consumption with decreasing efficiency factor is attributable to the round trip efficiency losses in the battery.
  - The rate clipping numbers used (2,2) mean the max RoC or RoD decreases linearly to zero when the SoC is at 50%. This has less of an effect than expected. The effect also decreases with decreasing efficiency factor. This is explain by the fact that with decreased efficiency the battery is used less and thus the SoC will deviate less from it's initial value of %50 of capacity (4kWh).

|$f\_d$|$(c\_d^{min},c\_d^{max})$|Cost|Total Demand
|-|-|-|-|
|-|-|7.94|24.31|
|1.00|-|6.14|24.31|
|0.95|-|6.40|25.06|
|0.90|-|6.68|25.73|
|0.80|-|7.22|27.20|
|1.00|(2,2)|6.30|24.31|
|0.95|(2,2)|6.50|24.95|
|0.90|(2,2)|6.72|25.66|
|0.80|(2,2)|7.25|26.96|


[Palizban2014]: http://www.sciencedirect.com/science/article/pii/S1364032114000264
[Gu2014]: http://www.sciencedirect.com/science/article/pii/S0142061513002883
[Jongerden2009]: https://ieeexplore.ieee.org/abstract/document/5346550
[Li2011]: https://ieeexplore.ieee.org/abstract/document/6039082/
[Xiaoping2010]: /
[Zhang2013Robust]: https://ieeexplore.ieee.org/abstract/document/6510507/
[Wei2018]: https://ieeexplore.ieee.org/abstract/document/8370675/
